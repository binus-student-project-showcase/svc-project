package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"

	"gitlab.com/binus-student-project-showcase/svc-project/api"
	apiv1 "gitlab.com/binus-student-project-showcase/svc-project/api/v1"
	"gitlab.com/binus-student-project-showcase/svc-project/setting"
	"gitlab.com/binus-student-project-showcase/svc-project/utils"
)

var mysqlparam setting.MysqlConn
var swaggerparam setting.SwaggerParam

func init() {

	getEnvVar := func(input string, output *int) {
		tmpval, err := strconv.Atoi(os.Getenv(input))
		*output = tmpval
		if err != nil {
			panic(fmt.Sprintf("Error in conversing %s with err of %s", input, err))
		}
	}

	// in prod, there should already be these env variables
	if len(os.Args) == 1 { // no additional arg
		os.Setenv("MYSQL_USERNAME", "user")
		os.Setenv("MYSQL_PASSWORD", "{ENTER MANUALLY}")
		os.Setenv("MYSQL_ADDRESS", "localhost")
		os.Setenv("MYSQL_DB_NAME", "project_db")
		os.Setenv("MYSQL_MaxIdleConns", "10")
		os.Setenv("MYSQL_MaxOpenConns", "100")
		os.Setenv("MYSQL_ConnMaxIdleTime", "5")
		os.Setenv("MYSQL_ConnMaxLifetime", "60")

		os.Setenv("SCHEMA", "http")
		os.Setenv("SERVER_NAME", "localhost")
		os.Setenv("PORT", "8080")
		os.Setenv("BASE_PATH", "api/v1")
	}

	mysqlparam.MYSQL_USERNAME = os.Getenv("MYSQL_USERNAME")
	mysqlparam.MYSQL_PASSWORD = os.Getenv("MYSQL_PASSWORD")
	mysqlparam.MYSQL_ADDRESS = os.Getenv("MYSQL_ADDRESS")
	mysqlparam.MYSQL_DB_NAME = os.Getenv("MYSQL_DB_NAME")
	getEnvVar("MYSQL_MaxIdleConns", &mysqlparam.MYSQL_MaxIdleConns)
	getEnvVar("MYSQL_MaxOpenConns", &mysqlparam.MYSQL_MaxOpenConns)
	getEnvVar("MYSQL_ConnMaxIdleTime", &mysqlparam.MYSQL_ConnMaxIdleTime)
	getEnvVar("MYSQL_ConnMaxLifetime", &mysqlparam.MYSQL_ConnMaxLifetime)

	// prepare swagger.json

	swaggerparam.SCHEMA = os.Getenv("SCHEMA")
	swaggerparam.SERVER_NAME = os.Getenv("SERVER_NAME")
	swaggerparam.PORT = os.Getenv("PORT")
	swaggerparam.BASE_PATH = os.Getenv("BASE_PATH")

	// TODO parse using json
	replaceStrings("swagger-ui-4.14.3/dist/swagger.json.template", "swagger-ui-4.14.3/dist/swagger.json")
	replaceStrings("swagger-ui-4.14.3/dist/swagger-initializer.js.template", "swagger-ui-4.14.3/dist/swagger-initializer.js")

}

func replaceStrings(inputPath string, outPath string) {
	input, err := ioutil.ReadFile(inputPath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	output := bytes.Replace(input, []byte("SCHEMA"), []byte(swaggerparam.SCHEMA), -1)
	output = bytes.Replace(output, []byte("SERVER_NAME"), []byte(swaggerparam.SERVER_NAME), -1)
	output = bytes.Replace(output, []byte("PORT"), []byte(swaggerparam.PORT), -1)
	output = bytes.Replace(output, []byte("BASE_PATH"), []byte(swaggerparam.BASE_PATH), -1)

	if err = ioutil.WriteFile(outPath, output, 0666); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func main() {

	db, _ := utils.GetConnection(mysqlparam)
	defer db.Close()

	// inject to apiv1
	apiv1.Db = db

	app := api.Api(db)

	app.Static("/", "./swagger-ui-4.14.3/dist")

	log.Fatal(app.Listen(fmt.Sprintf(":%s", swaggerparam.PORT)))
}
