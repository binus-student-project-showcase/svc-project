ARG GO_VERSION=1.18

FROM golang:${GO_VERSION}-alpine AS builder

RUN apk update && apk add alpine-sdk git && rm -rf /var/cache/apk/*

RUN mkdir -p /svc-project
WORKDIR /svc-project

COPY go.mod .
COPY go.sum .
RUN go mod tidy

COPY . .
RUN go build -o ./main ./main.go

FROM alpine:latest

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

RUN mkdir -p /svc-project
WORKDIR /svc-project
COPY --from=builder /svc-project/main .
COPY --from=builder /svc-project/swagger-ui-4.14.3 /svc-project/swagger-ui-4.14.3

EXPOSE 8080

ENTRYPOINT ["./main","prod"]
