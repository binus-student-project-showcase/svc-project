package v1

import (
	"context"
	"database/sql"
	"log"
	_ "fmt"
	"os"
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/binus-student-project-showcase/svc-project/model"
	"gitlab.com/binus-student-project-showcase/svc-project/repository"
	"gitlab.com/binus-student-project-showcase/svc-project/utils"
)

var Db *sql.DB

func HandleHealthz(c *fiber.Ctx) error {

	c.Status(fiber.StatusOK)
	return c.SendString("healthy")
}

func HandleToken(c *fiber.Ctx) error {

	credential := new(model.Credential)
	if err := c.BodyParser(credential); err != nil {
		log.SetPrefix("[LOG] ")
		log.Println("from HandleToken: ", err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	token, err := utils.MakePostRequest(os.Getenv("SVC_AUTH_URL")+"/token", *credential)
	if err != nil {
		log.SetPrefix("[LOG] ")
		log.Println("from HandleToken: ", err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	c.Status(fiber.StatusOK)
	return c.JSON(token)

}

func HandlePostAProject(c *fiber.Ctx) error {

	
	req := c.Context().Request

	authorizationHeader := string(req.Header.Peek("Authorization"))
	if authorizationHeader == "" {
		return fiber.NewError(fiber.StatusUnauthorized, "Authorization header is missing")
	}

	if !strings.HasPrefix(authorizationHeader, "Bearer ") {
		return fiber.NewError(fiber.StatusBadRequest, "Invalid Authorization header format")
	}

	token := strings.TrimPrefix(authorizationHeader, "Bearer ")


	statusCode, err := utils.VerifyToken(os.Getenv("SVC_AUTH_URL")+"/validity",token)
	if err != nil {
		log.SetPrefix("[LOG] ")
		log.Println("from HandleGetAllProjects: ", err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	if statusCode != 200 {
		return c.SendStatus(fiber.StatusUnauthorized)
	}

	p := new(model.Project)

	if err := c.BodyParser(p); err != nil {
		log.SetPrefix("[LOG] ")
		log.Println("from HandlePostAProject: ", err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	projectsRepository := repository.ImpProjectsRepository(Db)

	_, err := projectsRepository.Insert(context.Background(), *p)

	if err != nil {
		log.SetPrefix("[LOG] ")
		log.Println("from HandlePostAProject: ", err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	return c.SendStatus(fiber.StatusCreated)

}

func HandleGetAllProjects(c *fiber.Ctx) error {

	req := c.Context().Request

	authorizationHeader := string(req.Header.Peek("Authorization"))
	if authorizationHeader == "" {
		return fiber.NewError(fiber.StatusUnauthorized, "Authorization header is missing")
	}

	if !strings.HasPrefix(authorizationHeader, "Bearer ") {
		return fiber.NewError(fiber.StatusBadRequest, "Invalid Authorization header format")
	}

	token := strings.TrimPrefix(authorizationHeader, "Bearer ")


	statusCode, err := utils.VerifyToken(os.Getenv("SVC_AUTH_URL")+"/validity",token)
	if err != nil {
		log.SetPrefix("[LOG] ")
		log.Println("from HandleGetAllProjects: ", err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	if statusCode != 200 {
		return c.SendStatus(fiber.StatusUnauthorized)
	}


	projectsRepository := repository.ImpProjectsRepository(Db)

	projects, err := projectsRepository.FindAll(context.Background())

	if err != nil {
		log.SetPrefix("[LOG] ")
		log.Println("from HandleGetAllProjects: ", err.Error())
		return c.SendStatus(fiber.StatusBadRequest)
	}

	c.Status(fiber.StatusOK)
	return c.JSON(projects)

}
