package api

import (
	"database/sql"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"

	apiv1 "gitlab.com/binus-student-project-showcase/svc-project/api/v1"
)

func middleware(c *fiber.Ctx) error {
	return c.Next()
}

func handler(c *fiber.Ctx) error {
	c.Status(fiber.StatusOK)
	return c.SendString(c.Path())
}

func Api(db *sql.DB) *fiber.App {
	app := fiber.New()
	app.Use(cors.New())
	app.Use(logger.New())
	// app.Use(func(c *fiber.Ctx) error {
	// 	c.Locals("db", db)
	// 	return c.Next()
	// })

	// Root API route
	api := app.Group("/api", middleware) // /api

	// API v1 routes
	v1 := api.Group("/v1", middleware) // /api/v1

	v1.Get("/healthz", apiv1.HandleHealthz)

	v1.Post("/token", apiv1.HandleToken)

	/*
		curl -v -X GET "http://localhost:8080/api/v1/projects" \
		-H "Accept: application/json"
	*/
	v1.Get("/projects", apiv1.HandleGetAllProjects)

	/*
		curl -v -X POST "http://localhost:8080/api/v1/projects" \
		-H "Content-Type: application/json" \
		--data '{"project_name":"aa","owner":"cc"}'
	*/
	v1.Post("/projects", apiv1.HandlePostAProject)

	return app
}
