package utils

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"bytes"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"encoding/json"

	"gitlab.com/binus-student-project-showcase/svc-project/setting"
	"gitlab.com/binus-student-project-showcase/svc-project/model"

	_ "github.com/go-sql-driver/mysql"
)

func GetConnection(mysqlparam setting.MysqlConn) (*sql.DB, error) {
	MYSQL_CONN_URI := fmt.Sprintf("%s:%s@tcp(%s)/%s?parseTime=true",
		mysqlparam.MYSQL_USERNAME,
		mysqlparam.MYSQL_PASSWORD,
		mysqlparam.MYSQL_ADDRESS,
		mysqlparam.MYSQL_DB_NAME,
	)
	db, err := sql.Open("mysql", MYSQL_CONN_URI)
	if err != nil {
		log.SetPrefix("[Controller] [LOG] ")
		log.Println("Database connection is FAILED")
		return db, err
	}

	// Open doesn't open a connection. Validate DSN data:
	err = db.Ping()
	if err != nil {
		log.SetPrefix("[Controller] [LOG] ")
		log.Println("Database connection is FAILED")
		return db, err
	} else {
		log.SetPrefix("[Controller] [LOG] ")
		log.Println("Database connection is established")
	}

	db.SetMaxIdleConns(mysqlparam.MYSQL_MaxIdleConns)
	db.SetMaxOpenConns(mysqlparam.MYSQL_MaxOpenConns)
	db.SetConnMaxIdleTime(time.Duration(mysqlparam.MYSQL_ConnMaxIdleTime) * time.Minute)
	db.SetConnMaxLifetime(time.Duration(mysqlparam.MYSQL_ConnMaxLifetime) * time.Minute)

	return db, nil
}

func assignFieldIfExists(field *string, data map[string]interface{}, key string) error {
	value, ok := data[key]
	if !ok {
		return fmt.Errorf("key %q does not exist in the data", key)
	}

	strValue, ok := value.(string)
	if !ok {
		return fmt.Errorf("value for key %q is not a string", key)
	}

	*field = strValue
	return nil
}

func VerifyToken(urlAddress string, token string) (int, error) {

	payload := map[string]interface{}{
		"token": token,
	}


	// Convert the payload to JSON
	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		return 500, fmt.Errorf("Error marshaling JSON payload: %w", err)
	}

	// Create a request with the JSON payload
	req, err := http.NewRequest("POST", urlAddress, bytes.NewBuffer(jsonPayload))
	if err != nil {
		return 500, fmt.Errorf("Error creating HTTP request: %w", err)
	}

	// Set the appropriate headers
	req.Header.Set("Content-Type", "application/json")

	// Send the request
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return 500, fmt.Errorf("Error sending HTTP request: %w", err)
	}
	defer resp.Body.Close()

	
	return resp.StatusCode,nil

}

func MakePostRequest(urlAddress string, requestBody model.Credential) (model.Token, error) {

	form := url.Values{}
	form.Set("username", requestBody.Username)
	form.Set("password", requestBody.Password)
	body := strings.NewReader(form.Encode())

	// Create an HTTP client
	client := http.Client{}

	// Create an HTTP request
	req, err := http.NewRequest("POST", urlAddress, body)
	if err != nil {
		return model.Token{}, fmt.Errorf("error creating request: %w", err)
	}

	// Set the request content type header
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	// Send the HTTP request
	resp, err := client.Do(req)
	if err != nil {
		return model.Token{}, fmt.Errorf("error sending request: %w", err)
	}
	defer resp.Body.Close()

	// Read the response body
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.Token{}, fmt.Errorf("error reading response body: %w", err)
	}

	// var apiResponse model.Response
	var apiResponse map[string]interface{}
	err = json.Unmarshal(responseBody, &apiResponse)
	if err != nil {
		return model.Token{}, fmt.Errorf("error unmarshaling JSON response: %w", err)
	}
	
	
	// manually assign 
	var token model.Token

	err = assignFieldIfExists(&token.AccessToken, apiResponse, "access_token")
	if err != nil {
		return model.Token{}, fmt.Errorf("error creating request: %w", err)
	}

	err = assignFieldIfExists(&token.TokenType, apiResponse, "token_type")
	if err != nil {
		return model.Token{}, fmt.Errorf("error creating request: %w", err)
	}

	err = assignFieldIfExists(&token.Type, apiResponse, "type")
	if err != nil {
		return model.Token{}, fmt.Errorf("error creating request: %w", err)
	}

	err = assignFieldIfExists(&token.Name, apiResponse, "name")
	if err != nil {
		return model.Token{}, fmt.Errorf("error creating request: %w", err)
	}

	err = assignFieldIfExists(&token.ExpiryDate, apiResponse, "expiry_date")
	if err != nil {
		return model.Token{}, fmt.Errorf("error creating request: %w", err)
	}

	err = assignFieldIfExists(&token.Affiliation, apiResponse, "affiliation")
	if err != nil {
		return model.Token{}, fmt.Errorf("error creating request: %w", err)
	}
	
	return token, nil
}