package repository

import (
	"context"

	"gitlab.com/binus-student-project-showcase/svc-project/model"
)

type ProjectsRepository interface {
	Insert(ctx context.Context, project model.Project) (int64, error) // will return last inserted id and an error
	FindAll(ctx context.Context) ([]model.Project, error)
}
