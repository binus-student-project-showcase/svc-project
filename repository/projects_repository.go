package repository

import (
	"context"

	"database/sql"

	"gitlab.com/binus-student-project-showcase/svc-project/model"
)

func ImpProjectsRepository(db *sql.DB) ProjectsRepository {
	return &projectsRepositoryStructMethod{DB: db}
}

type projectsRepositoryStructMethod struct {
	DB *sql.DB
}

func (repository *projectsRepositoryStructMethod) Insert(ctx context.Context, project model.Project) (int64, error) {
	mysql_script := "INSERT INTO Projects(project_name,owner) VALUES (?,?)"
	result, err := repository.DB.ExecContext(ctx, mysql_script, project.ProjectName, project.Owner)

	if err != nil {
		return 0, err
	}

	lastInsertId, err := result.LastInsertId()

	if err != nil {
		return 0, err
	}

	return lastInsertId, nil

}

func (repository *projectsRepositoryStructMethod) FindAll(ctx context.Context) ([]model.Project, error) {
	mysql_script := "SELECT project_id, project_name, owner FROM Projects"
	rows, err := repository.DB.QueryContext(ctx, mysql_script)

	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var projects []model.Project
	for rows.Next() {
		project := model.Project{}
		rows.Scan(&project.ProjectId, &project.ProjectName, &project.Owner)
		projects = append(projects, project)
	}
	return projects, nil
}
