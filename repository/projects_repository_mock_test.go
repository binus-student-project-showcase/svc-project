package repository

import (
	"context"
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/binus-student-project-showcase/svc-project/model"
)

func TestProjectsInsertMock(t *testing.T) {

	ctx := context.Background()

	mockProjectsRepository := MockProjectsRepository(mock.Mock{})

	project := model.Project{
		ProjectId:   1,
		ProjectName: "My Awesome Project",
		Owner:       "Dummy Student",
	}

	validate := validator.New()
	err := validate.Struct(project)
	assert.Nil(t, err)

	mockProjectsRepository.Mock.On("Insert", ctx, project).Return(nil)

	lastInsertId, err := mockProjectsRepository.Insert(ctx, project)
	assert.Nil(t, err)
	assert.IsType(t, int64(1), lastInsertId)
}

func TestProjectsFindAllMock(t *testing.T) {

	ctx := context.Background()

	mockProjectsRepository := MockProjectsRepository(mock.Mock{})

	mockProjectsRepository.Mock.On("FindAll", ctx).Return(nil)

	_, err := mockProjectsRepository.FindAll(ctx)
	assert.Nil(t, err)
}
