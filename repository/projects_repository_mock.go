package repository

import (
	"context"
	"errors"

	"github.com/stretchr/testify/mock"
	"gitlab.com/binus-student-project-showcase/svc-project/model"
)

func MockProjectsRepository(Mock mock.Mock) *projectsRepositoryStructMethodMock {
	return &projectsRepositoryStructMethodMock{Mock: Mock}
}

type projectsRepositoryStructMethodMock struct {
	Mock mock.Mock
}

func (repository *projectsRepositoryStructMethodMock) Insert(ctx context.Context, project model.Project) (int64, error) {
	arguments := repository.Mock.Called(ctx, project)

	if arguments.Get(0) == nil {
		var lastInsertId int64 = 10
		return lastInsertId, nil
	} else {
		return 0, errors.New("error message")
	}

}

func (repository *projectsRepositoryStructMethodMock) FindAll(ctx context.Context) ([]model.Project, error) {
	arguments := repository.Mock.Called(ctx)

	if arguments.Get(0) == nil {
		return []model.Project{}, nil
	} else {
		return []model.Project{}, errors.New("error message")
	}
}
