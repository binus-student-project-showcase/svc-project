package repository

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"testing"

	"github.com/Pallinder/go-randomdata"
	"github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"
	"gitlab.com/binus-student-project-showcase/svc-project/model"
	"gitlab.com/binus-student-project-showcase/svc-project/setting"
	"gitlab.com/binus-student-project-showcase/svc-project/utils"
)

var mysqlparam setting.MysqlConn

func getEnvVar(input string, output *int) {
	tmpval, err := strconv.Atoi(os.Getenv(input))
	*output = tmpval
	if err != nil {
		panic(fmt.Sprintf("Error in conversing %s with err of %s", input, err))
	}
}

func init() {

	// in prod, there should already be these env variables
	os.Setenv("MYSQL_USERNAME", "user")
	os.Setenv("MYSQL_PASSWORD", "{ENTER MANUALLY}")
	os.Setenv("MYSQL_ADDRESS", "localhost")
	os.Setenv("MYSQL_DB_NAME", "project_db")
	os.Setenv("MYSQL_MaxIdleConns", "10")
	os.Setenv("MYSQL_MaxOpenConns", "100")
	os.Setenv("MYSQL_ConnMaxIdleTime", "5")
	os.Setenv("MYSQL_ConnMaxLifetime", "60")

	mysqlparam.MYSQL_USERNAME = os.Getenv("MYSQL_USERNAME")
	mysqlparam.MYSQL_PASSWORD = os.Getenv("MYSQL_PASSWORD")
	mysqlparam.MYSQL_ADDRESS = os.Getenv("MYSQL_ADDRESS")
	mysqlparam.MYSQL_DB_NAME = os.Getenv("MYSQL_DB_NAME")
	getEnvVar("MYSQL_MaxIdleConns", &mysqlparam.MYSQL_MaxIdleConns)
	getEnvVar("MYSQL_MaxOpenConns", &mysqlparam.MYSQL_MaxOpenConns)
	getEnvVar("MYSQL_ConnMaxIdleTime", &mysqlparam.MYSQL_ConnMaxIdleTime)
	getEnvVar("MYSQL_ConnMaxLifetime", &mysqlparam.MYSQL_ConnMaxLifetime)

}

func TestConnection(t *testing.T) {
	db, err := utils.GetConnection(mysqlparam)
	defer db.Close()

	assert.Nil(t, err)

}

func TestProjectsInsert(t *testing.T) {
	db, err := utils.GetConnection(mysqlparam)
	defer db.Close()
	assert.Nil(t, err)

	ctx := context.Background()

	projectsRepository := ImpProjectsRepository(db)

	project := model.Project{
		ProjectName: randomdata.Noun(),
		Owner:       randomdata.SillyName(),
	}

	validate := validator.New()
	err = validate.Struct(project)
	assert.Nil(t, err)

	lastInsertId, err := projectsRepository.Insert(ctx, project)
	assert.Nil(t, err)

	assert.IsType(t, int64(1), lastInsertId)
	fmt.Println("Last Inserted Id is: ", lastInsertId)
}

func TestProjectsFindAll(t *testing.T) {
	db, err := utils.GetConnection(mysqlparam)
	defer db.Close()
	assert.Nil(t, err)

	ctx := context.Background()

	projectsRepository := ImpProjectsRepository(db)

	projects, err := projectsRepository.FindAll(ctx)
	assert.Nil(t, err)

	fmt.Println("Test FindAll Success!", projects)
}
