package model

import (
	"testing"

	"github.com/go-playground/validator/v10"
)

func TestProjectModel(t *testing.T) {

	validate := validator.New()

	project := Project{
		ProjectId:   1,
		ProjectName: "My Awesome Project",
		Owner:       "Dummy Student",
	}

	err := validate.Struct(project)

	if err != nil {
		t.Error(err.Error())
	}

}
