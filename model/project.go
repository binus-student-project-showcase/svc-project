package model

type Project struct {
	ProjectId   int64  `json:"project_id" validate:"numeric"`
	ProjectName string `json:"project_name" validate:"max=255,min=1"`
	Owner       string `json:"owner" validate:"max=45,min=1"`
}
